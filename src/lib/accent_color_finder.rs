pub mod aero;

#[derive(Clone, Default, Debug)]
pub struct ColorRgb {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl ColorRgb {
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self { red, green, blue }
    }
}

/// Specifies domains to be used to quantize color channels
#[derive(Clone, Debug)]
pub struct ChannelDomains {
    pub red: ChannelDomain,
    pub green: ChannelDomain,
    pub blue: ChannelDomain,
}

impl ChannelDomains {
    pub fn create_evenly_spaced(amount: usize) -> Self {
        Self {
            red: ChannelDomain::create_evenly_spaced(amount),
            green: ChannelDomain::create_evenly_spaced(amount),
            blue: ChannelDomain::create_evenly_spaced(amount),
        }
    }
}

/// Specifies domains to be used to quantize color channels
#[derive(Clone, Debug)]
pub struct ChannelDomain(Vec<f32>);

impl ChannelDomain {
    /// Standard domain is 0.0 to 1.0.
    /// Each number inserts a split (the value of a split is part of the upper domain).
    /// Numbers must be in order (small to larger).
    /// Duplicates are forbidden.
    pub fn new(domains: Vec<f32>) -> Self {
        Self::try_new(domains).unwrap()
    }

    /// Standard domain is 0.0 to 1.0.
    /// Each number inserts a split (the value of a split is part of the upper domain).
    /// Numbers must be in order (small to larger).
    /// Duplicates are forbidden.
    pub fn try_new(domains: Vec<f32>) -> Result<Self, String> {
        let mut last = 0.0;
        for d in &domains {
            if *d <= 0.0 || *d >= 1.0 {
                return Err(format!("Domains contain out-of-bound split: {d}"));
            }

            if *d <= last {
                return Err("Domain order is invalid".to_string());
            }

            last = *d;
        }

        Ok(Self(domains))
    }

    pub fn create_evenly_spaced(amount: usize) -> Self {
        let mut splits = Vec::with_capacity(amount);
        for i in 1..amount {
            splits.push(i as f32 / amount as f32);
        }

        Self(splits)
    }

    /// Number of domains (not splits)
    #[allow(clippy::len_without_is_empty)]
    pub fn len(&self) -> usize {
        self.0.len() + 1
    }

    pub fn index_of_value(&self, value: f32) -> u8 {
        let mut current = 0;
        for split in &self.0 {
            if value < *split {
                return current;
            }

            current += 1;
        }

        current
    }
}

/// size_ means size, not highest value.
/// That means for a collection [a, b, c], size_ is 3.
fn to_1d(x: usize, y: usize, z: usize, size_x: usize, size_y: usize, _size_z: usize) -> usize {
    x + (y * size_x) + (z * size_x * size_y)
}

/// size_ means size, not highest value.
/// That means for a collection [a, b, c], size_ is 3.
fn to_3d(index: usize, size_x: usize, size_y: usize, _size_z: usize) -> (usize, usize, usize) {
    let x = index % size_x;
    let y = (index / size_x) % size_y;
    let z = index / (size_x * size_y);

    (x, y, z)
}

#[cfg(test)]
mod tests {
    use crate::accent_color_finder::{to_1d, to_3d};

    use super::ChannelDomain;

    #[test]
    fn index_conversion() {
        let mut counted_index = 0;

        for b in 0..=2 {
            for g in 0..=2 {
                for r in 0..=2 {
                    let index = to_1d(r, g, b, 3, 3, 3);
                    assert_eq!(index, counted_index, "counted index");
                    counted_index += 1;

                    let (rr, rg, rb) = to_3d(index, 3, 3, 3);

                    assert_eq!(r, rr, "r");
                    assert_eq!(g, rg, "g");
                    assert_eq!(b, rb, "b");
                }
            }
        }
    }

    #[test]
    fn domains() {
        let domains = ChannelDomain::new(vec![0.2, 0.8]);
        assert_eq!(0, domains.index_of_value(0.0));
        assert_eq!(0, domains.index_of_value(0.1));
        assert_eq!(1, domains.index_of_value(0.2));
        assert_eq!(1, domains.index_of_value(0.3));
        assert_eq!(1, domains.index_of_value(0.4));
        assert_eq!(1, domains.index_of_value(0.5));
        assert_eq!(1, domains.index_of_value(0.6));
        assert_eq!(1, domains.index_of_value(0.7));
        assert_eq!(2, domains.index_of_value(0.8));
        assert_eq!(2, domains.index_of_value(0.9));
        assert_eq!(2, domains.index_of_value(1.0));
    }
}
