use image::{DynamicImage, GenericImageView};

use super::{to_1d, to_3d, ChannelDomain, ChannelDomains, ColorRgb};

#[derive(Clone, Debug)]
pub struct AeroFinderOptions {
    /// Specifies domains to be used to quantize color channels
    channel_domains: ChannelDomains,
    ignore_grays: bool,

    /// Percentage of alpha (opacity) a pixel needs at least to be considered
    ignore_alpha_threshold: IgnoreAlphaThreshold,
}

#[derive(Clone, Debug)]
pub struct IgnoreAlphaThreshold(f32);

impl IgnoreAlphaThreshold {
    pub fn new(threshold: f32) -> Self {
        // ..1.0 being exclusive is wanted.
        if !(0.0..1.0).contains(&threshold) {
            panic!("ignore_alpha_threshold must be within [0.0, 1.0)");
        }

        Self(threshold)
    }

    fn threshold(&self) -> f32 {
        self.0
    }
}

impl AeroFinderOptions {
    pub fn new(channel_domains: ChannelDomains, ignore_grays: bool, ignore_alpha_threshold: IgnoreAlphaThreshold) -> Self {
        Self {
            channel_domains,
            ignore_grays,
            ignore_alpha_threshold,
        }
    }
}

impl Default for AeroFinderOptions {
    fn default() -> Self {
        const SPLIT_A: f32 = 60.0 / 255.0;
        const SPLIT_B: f32 = 200.0 / 255.0;
        let default_domains = ChannelDomain::new(vec![SPLIT_A, SPLIT_B]);

        Self {
            channel_domains: ChannelDomains {
                red: default_domains.clone(),
                green: default_domains.clone(),
                blue: default_domains,
            },
            ignore_grays: true,
            ignore_alpha_threshold: IgnoreAlphaThreshold::new(0.04),
        }
    }
}

impl AeroFinderOptions {
    pub fn set_channel_domains(&mut self, channel_domains: ChannelDomains) -> &Self {
        self.channel_domains = channel_domains;
        self
    }

    pub fn set_ignore_grays(&mut self, ignore_grays: bool) -> &Self {
        self.ignore_grays = ignore_grays;
        self
    }

    pub fn set_ignore_alpha_threshold(&mut self, ignore_alpha_threshold: IgnoreAlphaThreshold) -> &Self {
        self.ignore_alpha_threshold = ignore_alpha_threshold;
        self
    }
}

pub struct AeroAccentFinder;

impl AeroAccentFinder {
    /// Returns the top accent colors along with their score (that color's portion of the entire image)
    pub fn find_accent_color(image: &DynamicImage, options: &AeroFinderOptions) -> (ColorRgb, f32) {
        let max_buckets = Self::bucketize(image, options);

        let (width, height) = image.dimensions();
        let pixels = (width * height) as f32;

        max_buckets
            .iter()
            .max_by(|a, b| a.len().cmp(&b.len()))
            .map(|b| (Self::get_color_average_squared_rgb(b), b.len() as f32 / pixels))
            .unwrap_or_default()
    }

    /// Returns the top accent colors along with their score (that color's portion of the entire image)
    pub fn find_accent_colors(
        image: &DynamicImage,
        amount: usize,
        options: &AeroFinderOptions,
    ) -> Vec<(ColorRgb, f32)> {
        let mut max_buckets = Self::bucketize(image, options);

        let (width, height) = image.dimensions();
        let pixels = (width * height) as f32;

        max_buckets.sort_by_key(|b| std::cmp::Reverse(b.len()));
        max_buckets
            .iter()
            .take(amount)
            .map(|bucket| {
                (
                    Self::get_color_average_squared_rgb(bucket),
                    bucket.len() as f32 / pixels,
                )
            })
            .collect()
    }

    fn bucketize(image: &DynamicImage, options: &AeroFinderOptions) -> Vec<Vec<ColorRgb>> {
        let channel_domains = &options.channel_domains;
        let capacity = channel_domains.red.len() * channel_domains.green.len() * channel_domains.blue.len();
        let mut buckets: Vec<Vec<ColorRgb>> = Vec::with_capacity(capacity);
        for _ in 0..buckets.capacity() {
            buckets.push(Vec::new());
        }

        image.pixels().for_each(|(_x, _y, color)| {
            let [red, green, blue, alpha] = color.0;

            // Ignore pixels that are almost completely transparent
            let relative_alpha = alpha as f32 / 255.0;
            if relative_alpha < options.ignore_alpha_threshold.threshold() {
                return;
            }

            let index = Self::get_bucket_index(red, green, blue, options);
            buckets[index].push(ColorRgb::new(red, green, blue));
        });

        buckets
            .into_iter()
            .enumerate()
            // Filter away gray buckets
            .filter(|(index, _)| {
                if !options.ignore_grays {
                    return true;
                }

                let (bucket_red, bucket_green, bucket_blue) = to_3d(
                    *index,
                    channel_domains.red.len(),
                    channel_domains.green.len(),
                    channel_domains.blue.len(),
                );
                !(bucket_red == bucket_green && bucket_red == bucket_blue)
            })
            .map(|(_index, colors)| colors)
            .collect::<Vec<_>>()
    }

    fn get_bucket_index(red: u8, green: u8, blue: u8, options: &AeroFinderOptions) -> usize {
        let domains = &options.channel_domains;
        let red_bucket = domains.red.index_of_value(red as f32 / 255.0);
        let green_bucket = domains.green.index_of_value(green as f32 / 255.0);
        let blue_bucket = domains.blue.index_of_value(blue as f32 / 255.0);

        to_1d(
            red_bucket as usize,
            green_bucket as usize,
            blue_bucket as usize,
            domains.red.len(),
            domains.green.len(),
            domains.blue.len(),
        )
    }

    fn get_color_average_squared_rgb(colors: &[ColorRgb]) -> ColorRgb {
        let length = colors.len() as f32;
        let (squared_red, squared_green, squared_blue) = colors
            .iter()
            // Square each channel (as float)
            .map(|c| {
                (
                    c.red as f32 * c.red as f32,
                    c.green as f32 * c.green as f32,
                    c.blue as f32 * c.blue as f32,
                )
            })
            // Sum each channel
            .fold((0.0, 0.0, 0.0), |acc, (r, g, b)| (acc.0 + r, acc.1 + g, acc.2 + b));

        ColorRgb::new(
            (squared_red / length).sqrt() as u8,
            (squared_green / length).sqrt() as u8,
            (squared_blue / length).sqrt() as u8,
        )
    }
}
