use std::{error::Error, path::PathBuf, process::exit, time::Instant};

use accentor::accent_color_finder::{aero::{AeroAccentFinder, AeroFinderOptions, IgnoreAlphaThreshold}, ChannelDomains};
use clap::Parser;
use colored::Colorize;
use image::{imageops::FilterType, io::Reader, GenericImageView};

fn main() {
    let command = Command::parse();
    if let Err(e) = command.run() {
        eprintln!("Could not extract accent color: {e}");
    }
}

#[derive(Parser)]
struct Command {
    /// Path to image file
    file: PathBuf,

    /// Downsize the image before extracting the accent color (only if it's larger)
    #[arg(long)]
    downsize: Option<Option<u32>>,

    /// How many accent colors to extract (sorted by score)
    #[arg(short, long, default_value = "1")]
    number: u32,

    /// Don't ignore grey color buckets
    #[arg(long)]
    no_ignore_greys: bool,

    /// Ignore pixels that are almost completely transparent
    /// [default: 0.04}
    #[arg(long)]
    ignore_alpha_threshold: Option<f32>,

    /// Specify the amount of buckets per channel (each bucket is equal in size).
    /// Setting this too high results in might result in unexpected results!
    /// [default: 3]
    #[arg[long]]
    channel_domains_uniform_evenly_spaced: Option<usize>,

    /// Print more information
    #[arg(long)]
    verbose: bool,
}

impl Command {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        let start = Instant::now();
        let mut img = Reader::open(&self.file)?.decode()?;
        self.log(|| format!("Loaded image in {:?}", start.elapsed()));

        match self.downsize {
            None => (),
            Some(downsize_option) => {
                let downsize = downsize_option.unwrap_or(64);
                let (width, height) = img.dimensions();
                if downsize > 0 && (width > downsize || height > downsize) {
                    let start = Instant::now();
                    img = img.resize(downsize, downsize, FilterType::Nearest);
                    self.log(|| format!("Resized image in {:?}", start.elapsed()));
                }
            },
        }

        let options = self.build_aero_options();

        let start = Instant::now();
        let accents = if self.number == 1 {
            vec![AeroAccentFinder::find_accent_color(&img, &options)]
        } else {
            AeroAccentFinder::find_accent_colors(&img, self.number as usize, &options)
        };
        self.log(|| format!("Extracted accent in {:?}", start.elapsed()));

        for (accent, score) in accents {
            println!("Accent Color: ({score:.3}) {:?} {}", accent, "██".truecolor(accent.red, accent.green, accent.blue));
        }

        Ok(())
    }

    fn build_aero_options(&self) -> AeroFinderOptions {
        let mut options = AeroFinderOptions::default();

        if self.no_ignore_greys {
            options.set_ignore_grays(false);
        }

        if let Some(threshold) = self.ignore_alpha_threshold {
            options.set_ignore_alpha_threshold(IgnoreAlphaThreshold::new(threshold));
        }

        if let Some(value) = self.channel_domains_uniform_evenly_spaced {
            if !(2..=32).contains(&value) {
                eprintln!("channel-domains-uniform-evenly-spaced must be in range [2,32]");
                exit(2);
            }

            options.set_channel_domains(ChannelDomains::create_evenly_spaced(value));
        }

        options
    }

    fn log(&self, message_generator: impl FnOnce() -> String) {
        if self.verbose {
            println!("{}", message_generator());
        }
    }
}
